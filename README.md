# Enhancing GNN performance with ensemble strategies


## Objective

Predict compute-node anomalies using a set of GNN models trained for different time horizons, and employ ensemble strategies to enhance the performance of base models.

## Background
Supercomputers play a central role for technological advancements in our society. They are complex machines that are composed of heterogeneous components such as compute node, cooling infrastructure etc. Over the years the complexity of supercomputing systems has been increasing rapidly bringing in a series of big challenges for the facility managers, engineers and operators etc, particularly now as we are now approaching exascale. One of the main challenges faced is the unavailability of compute nodes resulting in unproductivity or monetary losses for the stakeholders. These problems can be avoided using predictive maintenance (anomaly detection and anomaly prediction) using Machine Learning (ML) and Artificial Intellingence (AI). 

In this project, we are working on the anomaly prediction task using Graph Neural Networks(GNN). GNNs are a class of neural networks designed to work directly on graph-structured data. They extend traditional neural network architectures to handle graph data, which is characterized by nodes (entities) and edges (relationships) connecting these nodes. These models are trained using Graph convolutional networks (GCN). GCNs are a type of GNN designed for learning from graph-structured data. They extend traditional neural network architectures to handle graphs, representing data as nodes and edges. GCNs leverage graph convolutional layers to perform message passing between neighboring nodes, enabling each node to aggregate information from its local neighborhood. This information is then used to update the node representations iteratively through multiple layers. 

## Ensemble
Ensemble learning in machine learning is a technique where multiple models are combined to improve performance. Instead of relying on a single model, ensemble methods use the collective predictions of multiple models to make more accurate decisions. Popular ensemble techniques include Bagging, Boosting, Stacking, and Voting. These methods leverage the strengths of different models to mitigate errors and enhance overall performance. Ensemble learning is widely used for its ability to achieve better generalization and robustness in various machine learning tasks.

Visit the following links for more detailed information:
 - https://www.sciencedirect.com/science/article/pii/S095219762200269X
 - https://www.analyticsvidhya.com/blog/2018/06/comprehensive-guide-for-ensemble-models/
 
## Models and dataset

In this repository, you will find the already trained anomaly prediction models for the Marconi100 supercomputer at CINECA in Italy. CINECA is equipped with a monitoring framework called Examon. It collects live telemetry data from heterogeneous data sources and provides storage in a NoSQL database allowing query capabilities using a dedicated library called ExamonQL. Marconi100 comes with a historic dataset of 31 months, which is open-source. It contains all the metrics collected over the time period and also contains the data collected from its compute nodes, which are 980 in total. For a more detailed explanation of the dataset, visit the following [link](https://www.nature.com/articles/s41597-023-02174-3).

From the whole dataset, the time-aggregated dataset of 15-minute intervals each is the most appropriate for the anomaly prediction task, and it can be found at the following [zenodo link](https://zenodo.org/records/7541722). Once you are at the link, you will see the complete dataset for all the racks in Marconi100, which is 49 in total. The total size of the dataset is 24.8GB, but for this project, you can download any one of the rack's data.

After downloading the data for a single rack, if you open the folder, you will see the files inside, which are the compute nodes housed inside that rack, and each node's data is stored in a Parquet file. The Parquet files contain the telemetry data from sensors on compute nodes.

In the folder 'GNN-Trained Models', you will find the pre-trained base models for various future time windows. Our models are trained for nine different time windows, enabling the prediction of anomaly occurrence probability from one hour ahead to 72 hours ahead. Since our dataset has 15-minute time intervals, the future windows would result in the following:

| Future window (timestamps) | Time  ahead (Hr) |
| ------ | ------ |
|    4    |    1    |
|    6    |    1.5  |
|    12   |    3    |
|    24   |    6    |
|    32   |    8    |
|    64   |    16   |
|    96   |    24   |
|    192  |    48   |
|    288  |    72   |

Each model receives sensor metric inputs and performs graph convolution to produce a final output, which is the probability of anomaly occurrence. The model structure is defined below:

Model:

- GCN(417, 300)
- GCN(300, 100)
- GCN(100, 16)
- Linear(16, 16)
- Linear(16, 1)

![Gnn_model](gnn_model.png)

For an in-depth detail about the models, you can read the paper at the following [link](https://dl.acm.org/doi/abs/10.1145/3578245.3585335).

## Base model performance

| (T+FW) | Time-ahead | AUC of GNN |
|--------|------------|------------|
| T+4    | 1hr ahead  | 0.9100     |
| T+6    | 1.5hr ahead| 0.8826     |
| T+12   | 3hr ahead  | 0.8676     |
| T+24   | 6hr ahead  | 0.8661     |
| T+32   | 8hr ahead  | 0.8454     |
| T+64   | 16hr ahead | 0.7801     |
| T+96   | 24hr ahead | 0.7317     |
| T+192  | 48hr ahead | 0.6455     |
| T+288  | 72hr ahead | 0.6219     |


The table displays the AUC (Area Under the Curve) scores achieved by a Graph Neural Network (GNN) model at various time-ahead intervals. The AUC metric is commonly used to evaluate the performance of binary classification models, where higher AUC values indicate better model performance.

From the table, we see that at a time-ahead interval of 1 hour ahead (T+4), the GNN model achieved an AUC score of 0.91, indicating strong predictive performance. The model's performance decreases as we increase the future window. Conversely, at longer time-ahead intervals such as 72 hours (T+288), the AUC score decreases to 0.6219, suggesting decreased predictive accuracy over longer forecast horizons.

## Task

You can find the notebook titled 'GNN-ensemble.ipynb' in this repository. This notebook contains the tasks required to complete the project. First, you will traverse the dataset to understand the telemetry data. Then, you will load the pre-trained models to perform model inferences, familiarizing yourself with the data and the GNN models.

Next, you will implement ensemble strategies to determine if these models can outperform the base models.

The notebook provides guidelines for implementing tasks, along with partially filled codes.

## Platform

You can work on your own personal PC's using any code editor (VSCODE or SublimeText etc), or work in the systems available in the Lab 7 at UNIBO. Or you may even just use Google Colab which offers a platform for running Jupyter notebooks.

## Contact
- Prof.Andrea Bartolini (a.bartolini@unibo.it)
- Junaid Ahmed Khan - Tutor (junaidahmed.khan@unibo.it)
